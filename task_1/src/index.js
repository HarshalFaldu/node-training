const dotenv = require("dotenv");
dotenv.config();
const express = require("express");
const bodyParser = require("body-parser");
var randomstring = require("randomstring");
const { response } = require("express");



const mail = process.env.MAIL;
const password = process.env.PASSWORD;
var urlencodedParser = bodyParser.urlencoded({ extended: false })  
const app = express();

const str  = __dirname;
const patha  = str.substring(0,str.lastIndexOf('\\'))
app.use(express.static(patha + '/public'));

app.get("/", function(req, res) {
    res.sendFile(patha + '/public' + "/index.html");
  });

app.post("/login",urlencodedParser,function(req, res) {  

    if(req.body.email === mail && req.body.password===password){
        res.redirect('/home');
        res.end("yes");
    } else {
        res.redirect('/');
        res.end("Invalid credentials")
    }
});

// app.use(express.static(patha + '/public'));
app.get("/home",function(req,res) {
    res.sendFile(patha+ '/public' + "/home.html");
})


app.post("/ajax",urlencodedParser,function(req, res) {      
    var response = {
        product_name : req.body.val,
        random_str : randomstring.generate(10)
    }
    res.send(response)
});

app.listen(3000,() => {
    console.log("Started on PORT 3000");
})