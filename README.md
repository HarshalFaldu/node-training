# Task 1 #

### Step 1 ###

* Clone the repository at the any directory
* Download node js if not downloaded earlier

### Step 2 ###

* Open terminal and go to the directory task_1 in terminal
* First of all you have to install all the dependencied.
* For that you have to run following command.
``` npm install ```

### Step 3 ###

* After installing all the dependencies now run the following command
``` node src/index.js  ```

### Login credentials ###

** email : weboccult@gmail.com **
** password : weboccult@123 **

### After login ###

* User can add products and the product list is shown below in the page without refreshing