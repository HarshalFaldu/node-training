$(document).on("click",".addMoreBlogs",function(){
    var i = $(".post-preview")
    apiData = {
        lastData: i.length
    }
    Api.post("api/addMoreBlogs",null,apiData,function(error,res){
        if(error){
            console.log(error);
        }else{
            console.log("API: ",res.postData);
            data = res.postData;
            for (let index = 0; index < data['blogs'].length; index++) {
                var str =  `<div class="post-preview">
                        <a href="post.html">
                            <h2 class="post-title">${data['blogs'][index]['title']}</h2>
                            <h3 class="post-subtitle">${data['blogs'][index]['description']}</h3>
                        </a>
                        <p class="post-meta">
                            Posted by
                            <a href="#!">${data['user'][index][0]['name']}</a>
                            on ${data['blogs'][index]['createdAt']}
                        </p>
                    </div>
                    <!-- Divider-->
                    <hr class="my-4" />`
                    $(".upper").before(str)
                    console.log($(".my-4"));
            }
            // window.location.href = "/index"
        }
    })
})