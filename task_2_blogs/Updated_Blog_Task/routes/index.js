class IndexRoute {
  constructor() {
    this.router = require("express").Router();
    this.middleware = require("../middelware");
    this.authController = require("../controllers/AuthController");
    this.dashboardController = require("../controllers/DashboardController");
    this.postController = require("../controllers/postController");
    // this.userController = require("../controllers/UserController");
    this.bodyParser = require("body-parser");
    this.urlencodedParser = this.bodyParser.urlencoded({ extended: false })  
    this.setRoutes();
  }
  setRoutes() {

    // POST  
    this.router.post(
      "/register_user",
      this.authController.register.bind(this.authController)
    );

    this.router.post(
      "/login_user",
      this.authController.login.bind(this.authController)
    );
    
    this.router.post(
      "/api/addPost",
      this.postController.addPost.bind(this.postController)
    );
    
    this.router.post(
      "/api/addMoreBlogs",
      this.dashboardController.addMore.bind(this.dashboardController)
    );
    
    // GET
    this.router.get(
      "/",
      this.middleware.redirectIfAuthenticated,
      this.dashboardController.home.bind(this.dashboardController)
    );

    this.router.get(
      "/logout",
      this.middleware.checkLogin,
      this.authController.logout.bind(this.authController)
    );
    
    this.router.get(
      "/add-post",
      this.middleware.checkLogin,
      this.dashboardController.addPost.bind(this.dashboardController)
    );
    
    this.router.get(
      "/about",
      this.middleware.checkLogin,
      this.dashboardController.about.bind(this.dashboardController)
    );
    
    this.router.get(
      "/post",
      this.middleware.checkLogin,
      this.dashboardController.post.bind(this.dashboardController)
    );

    this.router.get(
      "/contact",
      this.middleware.checkLogin,
      this.dashboardController.contact.bind(this.dashboardController)
    );

    this.router.get(
      "/index",
      this.middleware.checkLogin,
      this.dashboardController.home.bind(this.dashboardController)
    );

    this.router.get('/register', function(req, res, next) {
      res.render('register');
    });

  }
}
const router = new IndexRoute();
module.exports = router.router;