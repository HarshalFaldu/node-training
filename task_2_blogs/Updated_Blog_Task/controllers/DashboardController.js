const BaseController = require('./BaseController')
const config = require('../config/Config')

class DashboardController extends BaseController{
    constructor() {
        super();
        this.blogService = require('../services/blogService')
        this.es6BindAll = require('es6bindall')
        this.es6BindAll(this, ["home","about","post","contact","addPost","addMore"])
    }
        /**
     *
     * home 
     *
     * @param  {object}   request
     * @param  {object}   response
     * @return {view}      home
     */
    async home(req,res){
    
        var postData = await this.blogService.getBlogs(res,0)        
        res.render("index",{
            status: "logout",
            postData : JSON.stringify(postData),
        })
    }

    /**
   *
   * home 
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {view}      home
   */
    async addMore(req,res){
        // console.log(req.body);
        var postData = await this.blogService.getBlogs(res,req.body.lastData)        
        res.send({
            postData : postData,
        })
    }


     /**
   *
   * home 
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {view}      home
   */
    async about(req,res){
        res.render('about')
    }
     
    /**
    *
    * home 
    *
    * @param  {object}   request
    * @param  {object}   response
    * @return {view}      home
    */
    async post(req,res){
        res.render('post')
    }
    
    /**
    *
    * home 
    *
    * @param  {object}   request
    * @param  {object}   response
    * @return {view}      home
    */
    async addPost(req,res){
        res.render('add-post')
    }
    
    /**
    *
    * home 
    *
    * @param  {object}   request
    * @param  {object}   response
    * @return {view}      home
    */
    async contact(req,res){
        res.render('contact')
    }
}

module.exports = new DashboardController();