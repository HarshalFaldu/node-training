const BaseController = require('./BaseController')
class AuthController extends BaseController{
    constructor(params) {
        super();
        // this.path = require("path")
        this.router = require("express").Router();
        // this.app.set('views', this.path.join(__dirname, 'views'));
        // this.app.set('view engine', 'ejs');
        this.helperResponse = require('../helpers/HelperResponse')
        this.dashboardController = require('./DashboardController')
        this.middleware = require('../middelware')
        this.userService = require('../services/UserService')
        this.es6BindAll = require("es6bindall");
        this.es6BindAll(this, ["register","login","logout"]);
        this.loginValidation = this.joi.object({
            password : this.joi.string().required(),
            email : this.joi.string().email().required(),
            submit : ''
        });

        this.registrationValidation = this.joi.object({
            name : this.joi.string().required(),
            email : this.joi.string().email().required(),
            password : this.joi.string().required(),
            cpassword : this.joi.string().required(),
            submit : ''
        });
    }

    /**
   *
   * register
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {object}	  register
   */
    async register(req,res){
        var validation = this.registrationValidation.validate(req.body);
        if(validation.error){
            res.status(400).send({
                message: validation.error.details[0].message,
                type: "ValidationError",    
            });
        } else {
            if(req.body.password === req.body.cpassword){
                var user = await this.userService.registerUser(
                    res,
                    req.body
                );
                if(user.name){
                    res.redirect('/');
                } else {
                    res.status(401).send({
                      status: 400,
                      message: user.message,
                    });
                }
            } else {
                console.log("Password does not match");
                res.redirect('/register')
            }
        }
    }

    /**
   *
   * login
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {object}	  login
   */
    async login(req,res){
        var validation = this.loginValidation.validate(req.body);
        if(validation.error){
            res.status(400).send({
                message: validation.error.details[0].message,
                type: "ValidationError",
            });
        } else {
            var user = await this.userService.getUserByCred(
                res,
                req.body.email,
                req.body.password
            )
            if(user.status){
                this.helperResponse.setSession(req, "user", user.data.email)
                // console.log("In auth: ",req.session);
                // if true then return username
                // response['status'] = true 
                res.send({
                    "status": true
                })
                // console.log("true");
                // if false then error 400

                // this.router.get(
                //     "/index",
                //     this.middleware.checkLogin,
                //     this.dashboardController.home.bind(this.dashboardController)
                //   );
            }else{
                // response['status'] = false
                // console.log("false");
                res.send({
                    "status": false
                })
            }
        }
    }

    /**
   *
   * used for logout
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {object}	 redirect to home page
   */
  logout(req, res) {
    this.helperResponse.destroySession(req);
    res.redirect("/");
  }
}

module.exports = new AuthController();
