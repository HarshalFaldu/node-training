class BaseController{
    constructor() {
        this.mongoose = require('mongoose')
        this.joi = require('joi')
        this.bcrypt = require('bcrypt')
        this.helperResponse = require("../helpers/HelperResponse")
    }
    getUserId(req){
        const user = this.helperResponse.getSession(req,"user");
        return user ? user_id : 0;
    }
}

module.exports = BaseController;