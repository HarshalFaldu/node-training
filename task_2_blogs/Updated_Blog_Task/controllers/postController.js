const BaseController = require('./BaseController')
const config = require('../config/Config')

class postController extends BaseController{
    constructor() {
        super();
        this.router = require("express").Router();
        // this.app.set('views', this.path.join(__dirname, 'views'));
        // this.app.set('view engine', 'ejs');
        this.helperResponse = require('../helpers/HelperResponse')
        this.dashboardController = require('./DashboardController')
        this.middleware = require('../middelware')
        this.blogService = require('../services/blogService')
        this.es6BindAll = require("es6bindall");
        this.es6BindAll(this, ["addPost"]);
        
        this.postValidation = this.joi.object({
            title : this.joi.string().required(),
            description : this.joi.string().required(),
            content : this.joi.string().required()
        });
    }


    /**
   *
   * home 
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {view}      home
   */
    async addPost(req,res){
        console.log(req.session);
        var validation = this.postValidation.validate(req.body);
        if(validation.error){
            res.status(400).send({
                message: validation.error.details[0].message,
                type: "ValidationError",    
            });
        } else {
            var blog = await this.blogService.registerBlog(
                req,
                req.body
            )
            if(blog.title){
                console.log("Blog uploaded Successfully");
            }else {
                // res.status(401).send({
                //     status: 400,
                //     message: user.message,
                // });
                console.log("Blog uploaded Failed");
            }
        }
       
    }

    
     
    
}

module.exports = new postController();