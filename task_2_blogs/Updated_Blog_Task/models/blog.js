let mongoose = require('mongoose')

let blogSchema = new mongoose.Schema(
    {
        user_id : String,
        title: String,
        description: String,
        content: String
    },
    { timestamps: true }
)

module.exports = mongoose.model("blogs", blogSchema);