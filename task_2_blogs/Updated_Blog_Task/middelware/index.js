const helperResponse = require('../helpers/HelperResponse')

exports.redirectIfAuthenticated = (req,res,next) => {
    if(helperResponse.getSession(req,"user")){
        return res.redirect("/index");
    }else{
        res.render('login')
    }
}

exports.checkLogin = async function (req,res,next) {
    console.log("checkLogin",req.session);
    console.log("Inside");
    if(helperResponse.getSession(req,"user")){
        next();
    }else{
        return res.redirect('/');
    }
}
