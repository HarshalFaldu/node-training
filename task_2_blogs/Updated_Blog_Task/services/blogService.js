const bcrypt = require("bcrypt");
const helperResponse = require("../helpers/HelperResponse");
const blogModel = require("../models/blog");
const userModel = require("../models/users");

class blogService{
        /**
   *
   * Get user by user Id
   *
   * @param  {object}   request
   * @param  {object}   data
   * @return {object}
   */
   async registerBlog(req,data){
        console.log("Data in service",req.session);
        var blog =new blogModel({ user_id: req.session['user'], title: data.title,description:data.description,content:data.content})
        return blog.save()
        .catch((err) => {return err})
    }

    /*
    * Get user by user Id
    *
    * @param  {object}   response
    * @return {object}
    */

    async getBlogs(res,data){
        var allBlogs = await blogModel.find({}).sort({title:1}).skip(data).limit(2)
        console.log(allBlogs);
        var user = [];
        for (let index = 0; index < allBlogs.length; index++) {
            var a = await userModel.find({email:new RegExp('^'+allBlogs[index]['user_id']+'$', "i")})
            user.push(a)
        }
            
        return {
            blogs: allBlogs,
            user: user
        }
    }

}

module.exports = new blogService();