class HelperResponse{
    constructor() {
        this.config = require("../config/Config")
        this.md5 = require("md5")
    }

    transformRequest(data) {
        const transformRequest = (data = {}) =>
            Object.entries(data)
                .map(x => `${encodeURIComponent(x[0])}=${encodeURIComponent(x[1])}`)
                .join("&")
        return transformRequest(data)
    }

    /**
     *
     * get session
     *
     * @param  {string}  key
     * @return {object}
     */
    getSession(req, key) {
        return req.session[key]
    }

    /**
     *
     * set session
     *
     * @param  {string}  key
     * @param  {object}  value
     * @return {object}
     */
    setSession(req, key, value) {
        console.log(req.session);
        req.session[key] = value
    }

    /**
     *
     * detroy session
     * @param  {object}  request
     * @return {object}
     */
    destroySession(req) {
        req.session.destroy()
    }
}

module.exports = new HelperResponse()