const BaseController = require('./BaseController')
class AuthController extends BaseController{
    constructor(params) {
        super();
        this.helperResponse = require('../helpers/HelperResponse')
        this.userService = require('../services/UserService')
        this.es6BindAll = require("es6bindall");
        this.es6BindAll(this, ["register","login"]);
        this.loginValidation = this.joi.object({
            password : this.joi.string().required(),
            email : this.joi.string().email().required(),
            submit : ''
        });

        this.registrationValidation = this.joi.object({
            name : this.joi.string().required(),
            email : this.joi.string().email().required(),
            password : this.joi.string().required(),
            cpassword : this.joi.string().required(),
            submit : ''
        });
    }

    /**
   *
   * register
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {object}	  register
   */
    async register(req,res){
        var validation = this.registrationValidation.validate(req.body);
        if(validation.error){
            res.status(400).send({
                message: validation.error.details[0].message,
                type: "ValidationError",    
            });
        } else {
            if(req.body.password === req.body.cpassword){
                var user = await this.userService.registerUser(
                    res,
                    req.body
                );
                if(user.name){
                    res.redirect('/');
                } else {
                    res.status(401).send({
                      status: 400,
                      message: user.message,
                    });
                }
            } else {
                console.log("Password does not match");
                res.redirect('/register')
            }
        }
    }

    /**
   *
   * login
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {object}	  login
   */
    async login(req,res){
        var validation = this.loginValidation.validate(req.body);
        if(validation.error){
            res.status(400).send({
                message: validation.error.details[0].message,
                type: "ValidationError",
            });
        } else {
            var user = await this.userService.getUserByCred(
                res,
                req.body.email,
                req.body.password
            )
            if(user.status){
                // this.helperResponse.setSession(req, "user", user.data.email)
                res.redirect("/dashboard")
            }
        }
    }
}

module.exports = new AuthController();
