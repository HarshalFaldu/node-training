class IndexRoute {
  constructor() {
    this.router = require("express").Router();
    // this.middleware = require("../middleware");
    this.authController = require("../controllers/AuthController");
    // this.dashboardController = require("../controllers/DashboardController");
    // this.userController = require("../controllers/UserController");
    this.bodyParser = require("body-parser");
    this.urlencodedParser = this.bodyParser.urlencoded({ extended: false })  
    this.setRoutes();
  }
  setRoutes() {

    this.router.get('/', function(req, res, next) {
      res.render('login');
    });

    // Registration 
    this.router.post(
      "/register_user",
      this.authController.register.bind(this.authController)
    );

    this.router.post(
      "/login_user",
      this.authController.login.bind(this.authController)
    );

    this.router.get('/register', function(req, res, next) {
      res.render('register');
    });

  }
}
const router = new IndexRoute();
module.exports = router.router;