const helperResponse = require('../helpers/HelperResponse')

exports.redirectIfAuthenticated = (req,res,next) => {
    if(helperResponse.getSession(req,"user")){
        return res.redirect("/index");
    }else{
        next();
    }
}

exports.checkLogin = async function (req,res,next) {
    if(helperResponse.getSession(req,"user")){
        next();
    }else{
        return res.redirect('/');
    }
}
