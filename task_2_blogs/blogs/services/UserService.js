const bcrypt = require("bcrypt");
const helperResponse = require("../helpers/HelperResponse");
const userModel = require("../models/users");


class UserService{

    /**
   *
   * Get user by user Id
   *
   * @param  {object}   response
   * @param  {object}   request
   * @return {object}
   */
   async registerUser(res,data){
       console.log("Data in service",data);
        return bcrypt.hash(data.password, 10).then((hash)=>{
            var user =new userModel({ name: data.name, email: data.email,password:hash})
            return user.save()
        })
        .catch((err) => {return err})
   }

     /**
   *
   * Get user by user email and password
   *
   * @param  {object}   response
   * @param  {string}   email
   * @return {object}
   */
  async getUserByCred(res, email, password) {
    return new Promise(async function (resolve, reject) {
      await userModel
        .findOne({
          email: email,
        })
        .exec()
        .then(function (doc) {
          if (doc) {
            bcrypt.compare(password, doc.password, function (err, ress) {
              if (err) {
                resolve({
                  message: "Database error, Please try again",
                  status: false,
                });
              }
              if (ress) {
                resolve({ status: true, data: doc });
              } else {
                resolve({
                  message: "Incorrect Email and Password",
                  status: false,
                });
              }
            });
          } else {
            resolve({
              message: "Incorrect Email and Password",
              status: false,
            });
          }
        });
    });
  }
}

module.exports = new UserService();