var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res) {
   res.sendfile('./public/index.html');
});

var users = []
var roomno = 1;
io.on('connection', function(socket) {
   console.log("A user connected");

   socket.on('setUserName',function(data){
      if(users.indexOf(data)> -1){
         socket.emit('userExists',data + 'Username is taker, try some other names')
      } else {
         users.push(data);
         socket.emit('userSet',{username: data});
      }
   })

   socket.on('msg',function(data){
      io.sockets.emit('newmsg',data)
   })
})

http.listen(3000, function() {
   console.log('listening on localhost:3000');
});